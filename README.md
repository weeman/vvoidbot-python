# VVoidbot Python

pipenv:

```shell
pipenv install
pipenv shell
```

venv:

```shell
python -m venv ./venv
source ./venv/bin/activate
pip install -r requirements.txt
```

danach:

```shell
cp botconf.py.example botconf.py
$VISUAL botconf.py
python bot.py
```
